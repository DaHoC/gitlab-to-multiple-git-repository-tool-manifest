/**
 * This tool queries the Gitlab GraphQL API to retrieve projects matching a search criteria and converts the resulting project repositories list to a "Multiple Git Repository Tool" manifest.
 * Go to the GitLab instance page (i.e. https://gitlab.com) first to avoid CORS and certificate issues.
 *
 * This userscript can be copied to the browser Javascript console or used with i.e. Greasemonkey.
 */
"use strict";

let messageArea = null;

const retrieveGitlabProjectsGraphQL = async (gitlabApiPath, gitlabPersonalAccessToken = null, gitlabProjectsBasePath, searchTerm = null, membership = true, filterArchivedProjects = true) => {
    let allProjectsJson = [];
    let lastCursor = null;
    let hasNextPage = false;
    do {
        const gitlabProjectsResponseJson = await retrieveGitlabProjectsPageGraphQL(gitlabApiPath, gitlabPersonalAccessToken, lastCursor, searchTerm, membership);
        console.trace(gitlabProjectsResponseJson);
        // TODO: Check if keys exist
        const dataProjectsResponseJson = gitlabProjectsResponseJson.data.projects.nodes;
        let responseProjectsLength = dataProjectsResponseJson.length;
        allProjectsJson = allProjectsJson.concat(dataProjectsResponseJson);
        hasNextPage = gitlabProjectsResponseJson.data.projects.pageInfo.hasNextPage;
        if (hasNextPage) {
            lastCursor = gitlabProjectsResponseJson.data.projects.pageInfo.endCursor;
        }
        console.info(`Retrieved ${responseProjectsLength} projects in single request`);
        // TODO: Fix this loop for non-project responses (i.e. invalid PAT)
        // TODO: Implement some upper limit of projects to pull
    } while (hasNextPage);
    const allProjectsLength = allProjectsJson.length;
    const filteredProjectsJson = filterArchivedProjects ? filterArchived(allProjectsJson) : allProjectsJson;
    const filteredProjectsLength = filteredProjectsJson.length;
    logToMessageArea(`Retrieved ${allProjectsLength} projects in total, after filtering out archived projects ${filteredProjectsLength}`);
    sortByFullPath(filteredProjectsJson);
    const manifestXml = convertGitlabEntriesToXmlManifestFormat(gitlabProjectsBasePath, filteredProjectsJson);
    console.trace(manifestXml);
    return manifestXml;
};

const retrieveGitlabProjectsPageGraphQL = async (gitlabApiPath, gitlabPersonalAccessToken = null, lastCursor = null, searchTerm = null, membership = true) =>
    new Promise(function (resolve, reject) {
        // See https://docs.gitlab.com/ee/api/graphql/getting_started.html
        // See https://docs.gitlab.com/ee/api/graphql/reference/index.html#queryprojects
        // See https://docs.gitlab.com/ee/api/graphql/reference/index.html#connection-pagination-arguments
        const gitlabProjectsRequest = new XMLHttpRequest();
        gitlabProjectsRequest.open("POST", `${gitlabApiPath}`, true);
        gitlabProjectsRequest.setRequestHeader("Authorization", `Bearer ${gitlabPersonalAccessToken}`);
        gitlabProjectsRequest.setRequestHeader("Accept", "application/json");
        gitlabProjectsRequest.setRequestHeader("Content-Type", "application/json");
        gitlabProjectsRequest.responseType = "json";
        //gitlabProjectsRequest.setRequestHeader("Access-Control-Allow-Origin", "*");
        //gitlabProjectsRequest.setRequestHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        let searchParameter = "";
        if (searchTerm !== null) {
            // Replace quotes in searchTerm, preventing code injection
            const sanitizedSearchTerm = searchTerm.replace('\"', '\\\"');
            searchParameter = `, search: "${sanitizedSearchTerm}"`;
        }
        let cursorParameter = "";
        if (lastCursor !== null) {
            cursorParameter = `, after: "${lastCursor}"`;
        }
        const membershipParameter = `, membership: ${membership}`;
        // const sortParameter = `, sort: "fullPath_asc"`; // NOTE: Does not work

        const projectParameters = `searchNamespaces: true ${searchParameter} ${cursorParameter} ${membershipParameter}`;
        console.debug(projectParameters);
        const queryBodyJson = {
            query: `{
            projects(${projectParameters}) {
                nodes {
                    id
                    name
                    nameWithNamespace
                    topics
                    group {
                        id
                        fullName
                        fullPath
                    }
                    path
                    fullPath
                    httpUrlToRepo
                    sshUrlToRepo
                    repository {
                        rootRef
                    }
                    webUrl
                    archived
                }
                pageInfo {
                    endCursor
                    hasNextPage
                }
            }
        }`
        };
        console.debug(JSON.stringify(queryBodyJson));

        gitlabProjectsRequest.addEventListener('load', function (_) {
            if (gitlabProjectsRequest.status >= 200 && gitlabProjectsRequest.status < 300) {
                const gitlabProjectsResponseJson = gitlabProjectsRequest.response;
                console.log(gitlabProjectsResponseJson);
                resolve(gitlabProjectsResponseJson);
            } else {
                console.error(gitlabProjectsRequest.statusText, gitlabProjectsRequest.response);
                reject({
                    status: gitlabProjectsRequest.status,
                    statusText: gitlabProjectsRequest.statusText,
                    response: gitlabProjectsRequest.response
                })
            }
        });

        gitlabProjectsRequest.send(JSON.stringify(queryBodyJson));
    });

const convertGitlabEntriesToXmlManifestFormat = (gitlabProjectsBasePath, gitlabProjectsJson) => {
    const NEW_LINE = "\r\n";
    let xmlManifest = `<?xml version="1.0" encoding="UTF-8"?>` + NEW_LINE;
    xmlManifest += `<manifest>` + NEW_LINE;
    xmlManifest += `<remote name="gitlab_remote" fetch="${gitlabProjectsBasePath}"/>` + NEW_LINE;
    xmlManifest += `<default revision="master" remote="gitlab_remote" />` + NEW_LINE;
    xmlManifest += `<!-- project list -->` + NEW_LINE;
    gitlabProjectsJson.forEach(projectJson => {
        if (projectJson !== null &&
            projectJson.repository !== null &&
            projectJson.repository.rootRef !== null &&
            projectJson.httpUrlToRepo !== null &&
            projectJson.fullPath !== null) { // Filter null/unset project values, esp. branches
            const pathWithoutPrecedingProjectBaseUrl = projectJson.httpUrlToRepo.substring(gitlabProjectsBasePath.length); // Cut off base path
            const xmlEntry = `<project name="${pathWithoutPrecedingProjectBaseUrl}" path="${projectJson.fullPath}" revision="${projectJson.repository.rootRef}" />`;
            xmlManifest += xmlEntry + NEW_LINE;
        }
    });
    xmlManifest += "</manifest>";
    return xmlManifest;
}

const sortByFullPath = (gitlabProjectsJson) => {
    gitlabProjectsJson.sort((a, b) => a.fullPath.localeCompare(b.fullPath));
}

const filterArchived = (gitlabProjectsJson) => {
    return gitlabProjectsJson.filter(projectNode => !projectNode.archived);
}

const logToMessageArea = (message) => {
    if (messageArea) {
        const singleLogMessageDiv = document.createElement("div");
        singleLogMessageDiv.innerText = message;
        messageArea.append(singleLogMessageDiv);
    }
}

const createGUI = () => {
    const doc = document.open();
    const html = document.createElement("html");
    doc.append(html);

    const head = document.createElement("head");
    const style = document.createElement("style");
    style.innerText = "body {color: #333333; display: flex; flex: 1 1 auto; flex-direction: column;} \n"
        + "a {margin: auto; padding: 1em;} \n"
        + "button {height: 2em; margin-bottom: 5px; position: relative; background: #009579; border: none; outline: none; border-radius: 2px;} \n"
        + ".button-loading::after {content: '';position: absolute;width: 16px;height: 16px;top: 0;left: 0; right: 5em; bottom: 0; margin: auto; border: 4px solid transparent; border-top-color: #ffffff; border-radius: 50%; animation: button-loading-spinner 1s ease infinite;} \n"
        + "@keyframes button-loading-spinner {from {transform: rotate(0turn);} to {transform: rotate(1turn);}}";
    head.append(style);
    html.append(head);

    const body = document.createElement("body");
    html.append(body);
    const h1 = document.createElement("h1");
    h1.innerHTML = "Gitlab Projects to 'Multiple Git Repository Tool' Manifest";
    body.append(h1);
    // Put input fields into a form, use <form action="#" onsubmit="return retrieveGitlabProjects(this);">
    //const userForm = document.createElement("form");
    // TODO: Fieldset
    // userForm.setAttribute("action", "#");
    // userForm.setAttribute("onsubmit", "return retrieveGitlabProjectsViaForm(this);");

    const inputGitlabApiPath = document.createElement("input");
    inputGitlabApiPath.id = "inputGitlabApiPath";
    inputGitlabApiPath.value = `${window.location.origin}/api/graphql`;
    const labelForInputGitlabApiPath = document.createElement("label");
    labelForInputGitlabApiPath.htmlFor = inputGitlabApiPath.id;
    labelForInputGitlabApiPath.innerText = "GitLab instance GraphQL API path: ";
    body.append(labelForInputGitlabApiPath);
    body.append(inputGitlabApiPath);

    const inputGitlabProjectBasePath = document.createElement("input");
    inputGitlabProjectBasePath.id = "inputGitlabProjectBasePath";
    inputGitlabProjectBasePath.value = `${window.location.origin}/`;
    const labelForInputGitlabProjectBasePath = document.createElement("label");
    labelForInputGitlabProjectBasePath.htmlFor = inputGitlabProjectBasePath.id;
    labelForInputGitlabProjectBasePath.innerText = "GitLab projects base path: ";
    body.append(labelForInputGitlabProjectBasePath);
    body.append(inputGitlabProjectBasePath);

    // From https://gitlab.com/-/profile/personal_access_tokens requires PAT with scope "api read_api"
    const inputGitlabPersonalAccessToken = document.createElement("input");
    inputGitlabPersonalAccessToken.id = "inputGitlabPersonalAccessToken";
    inputGitlabPersonalAccessToken.type = "password";
    const labelForInputGitlabPersonalAccessToken = document.createElement("label");
    labelForInputGitlabPersonalAccessToken.htmlFor = inputGitlabPersonalAccessToken.id;
    labelForInputGitlabPersonalAccessToken.innerText = "GitLab Personal Access Token (PAT): ";
    body.append(labelForInputGitlabPersonalAccessToken);
    body.append(inputGitlabPersonalAccessToken);

    const inputProjectSearchTerm = document.createElement("input");
    inputProjectSearchTerm.id = "inputProjectSearchTerm";
    inputProjectSearchTerm.name = "inputProjectSearchTerm";  // to allow browser auto-completion/auto-fill
    inputProjectSearchTerm.setAttribute("autocomplete", "on");  // to allow browser auto-completion/auto-fill
    const labelForInputProjectSearchTerm = document.createElement("label");
    labelForInputProjectSearchTerm.htmlFor = inputProjectSearchTerm.id;
    labelForInputProjectSearchTerm.innerText = "GitLab projects search term: ";
    body.append(labelForInputProjectSearchTerm);
    body.append(inputProjectSearchTerm);

    const containerForInputProjectMembership = document.createElement("div");
    const inputProjectMembership = document.createElement("input");
    inputProjectMembership.id = "inputProjectMembership";
    inputProjectMembership.type = "checkbox";
    inputProjectMembership.checked = true;
    const labelForInputProjectMembership = document.createElement("label");
    labelForInputProjectMembership.htmlFor = inputProjectMembership.id;
    labelForInputProjectMembership.innerText = "Only list projects that the PAT is member of? ";
    containerForInputProjectMembership.append(labelForInputProjectMembership);
    containerForInputProjectMembership.append(inputProjectMembership);
    body.append(containerForInputProjectMembership);

    const containerForInputProjectArchivedFilter = document.createElement("div");
    const inputProjectArchivedFilter = document.createElement("input");
    inputProjectArchivedFilter.id = "inputProjectArchivedFilter";
    inputProjectArchivedFilter.type = "checkbox";
    inputProjectArchivedFilter.checked = true;
    const labelForInputProjectArchivedFilter = document.createElement("label");
    labelForInputProjectArchivedFilter.htmlFor = inputProjectArchivedFilter.id;
    labelForInputProjectArchivedFilter.innerText = "Filter out archived projects? ";
    containerForInputProjectArchivedFilter.append(labelForInputProjectArchivedFilter);
    containerForInputProjectArchivedFilter.append(inputProjectArchivedFilter);
    body.append(containerForInputProjectArchivedFilter);

    const linkManifestDownload = document.createElement('a');
    linkManifestDownload.target = "_blank";
    linkManifestDownload.innerText = "default.xml";
    linkManifestDownload.style.display = 'none';

    const triggerGitlabProjectsRetrievalAndConversionButton = document.createElement("button");
    const buttonInlineSpan = document.createElement("span");
    buttonInlineSpan.innerText = "Start";
    triggerGitlabProjectsRetrievalAndConversionButton.append(buttonInlineSpan);
    const triggerGitlabProjectsRetrievalAndConversionButtonClickEventListener = (_) => {
        triggerGitlabProjectsRetrievalAndConversionButton.disabled = true;
        triggerGitlabProjectsRetrievalAndConversionButton.classList.add('button-loading');
        const gitlabApiPath = inputGitlabApiPath.value;
        const gitlabProjectBasePath = inputGitlabProjectBasePath.value;
        const gitlabPersonalAccessToken = inputGitlabPersonalAccessToken.value;
        const gitlabProjectSearchTerm = (inputProjectSearchTerm.value.length === 0 ? null : inputProjectSearchTerm.value);
        const gitlabProjectMembership = inputProjectMembership.checked;
        const gitlabProjectFilterArchived = inputProjectArchivedFilter.checked;

        const manifestXmlPromise = retrieveGitlabProjectsGraphQL(gitlabApiPath, gitlabPersonalAccessToken, gitlabProjectBasePath, gitlabProjectSearchTerm, gitlabProjectMembership, gitlabProjectFilterArchived);
        manifestXmlPromise.then(manifestXml => {
            linkManifestDownload.href = "data:text/plain;charset=utf-8," + encodeURIComponent(manifestXml);
            linkManifestDownload.download = "default.xml";
            linkManifestDownload.style.display = 'block';
        }).catch(error => {
            console.error(error);
            logToMessageArea(error);
        }).finally(
            () => {
                triggerGitlabProjectsRetrievalAndConversionButton.disabled = false;
                triggerGitlabProjectsRetrievalAndConversionButton.classList.remove('button-loading');
            }
        );
    }
    triggerGitlabProjectsRetrievalAndConversionButton.addEventListener("click", triggerGitlabProjectsRetrievalAndConversionButtonClickEventListener);
    body.append(triggerGitlabProjectsRetrievalAndConversionButton);

    messageArea = document.createElement("div");
    messageArea.classList.add("messages");
    body.append(messageArea)

    body.appendChild(linkManifestDownload);

}

createGUI();
